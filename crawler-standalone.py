import requests
from bs4 import BeautifulSoup

url = "http://books.toscrape.com/"
max_depth = 3
scrape_queue = []
image_list = []
scrape_queue.append(url)
scrape_queue.append(None)
current_depth = 0

while(scrape_queue and current_depth <= max_depth):
    cur_url = scrape_queue.pop(0)
    if cur_url == None:
        current_depth += 1
    else:
        page = requests.get(cur_url)
        soup = BeautifulSoup(page.text, 'html.parser')

        images = soup.find_all('img')

        for image in images:
            try:
                if url + image['src'] not in image_list:
                    image_list.append(url + image['src'])
            except:
                pass

        ahrefs = soup.find_all('a')

        for a in ahrefs:
            try:
                if url + a['href'] not in ahrefs:
                    scrape_queue.append(url + a['href'])
            except:
                pass
        scrape_queue.append(None)

        print("{} images at depth {}".format(len(image_list), current_depth))

