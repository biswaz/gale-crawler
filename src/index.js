import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'
import {
    Grid, Row, Col
} from 'react-bootstrap'


class NameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: '' , url : '', depth: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        if(event.target.name === "url"){
            this.setState({ url: event.target.value });
            console.log('url')
        }
        else{
            this.setState({ depth: event.target.value });
            console.log('depth')
        }
        
    }

    handleSubmit(event) {
        // alert('A name was submitted: ' + this.state.value);
        axios.get('http://127.0.0.1:8000/scrape/?url=' + this.state.url + '&depth=' + this.state.depth )
            .then(response => this.setState({ data: response.data }))
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Enter seed url: 
                     <input type="text" value={this.state.url} onChange={this.handleChange} placeholder="http://www.google.com" name="url"/>
                </label><br />
                <label>
                    Max depth: 
                    <input type="text" value={this.state.depth} onChange={this.handleChange} placeholder="2" name="depth"/>
                </label>
                <input type="submit" value="Submit" />
            <p>{this.state.data}</p>
            </form>
        );
    }
}
 
class ImageViewer extends React.Component{
    constructor(props) {
        super(props);
        this.state = { data : []};
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/images/')
            .then(response => this.setState({ data: JSON.parse(JSON.stringify(response.data))}))
    }

    render(){

        const { data } = this.state;
        return(
            <div>
                Images scraped:<br />
                <ul>
                    {data.map(job =>
                        <li>{job[0]}<br />
                            {job[1].map(images =>
                                <img src={images.url} />
                            )}
                        </li>
                    )}
                </ul>
            </div>
        )
    }
}

class App extends React.Component{
    render(){
        return(
            <div>
            <Grid>
                    <Row className="show-grid">
                        <Col>
                            <NameForm   />
                            <ImageViewer />
                        </Col>
                    </Row>
            </Grid>
            </div>
        )
    }
}
// ========================================

ReactDOM.render(
    <App />,
    document.getElementById('root')
);