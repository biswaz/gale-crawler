from django.db import models

class Job(models.Model):
    url = models.CharField(max_length=200)

    def  __str__(self):
        return self.url


class Image(models.Model):
    url = models.CharField(max_length=200)
    job_name = models.ForeignKey(to=Job, on_delete=models.CASCADE)

    def  __str__(self):
        return self.url
