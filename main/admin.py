from django.contrib import admin
from main.models import Job, Image

admin.site.register(Job)
admin.site.register(Image)
