from django.shortcuts import render
from django.http import HttpResponse
from main.rq import scrape_queue
from main.scraper import scrape
from django.http import JsonResponse
from django.core import serializers
from main.models import Job, Image

def scraper_view(request):
    input_url = request.GET.get('url')
    depth = request.GET.get('depth')
    scrape_queue.enqueue(scrape, input_url, depth)
    return(HttpResponse("Url queued"))

"""
The api limits to 10 images per job to reduce clutter
"""
def image_view(request):
    jobs = Job.objects.all()
    images = Image.objects.all()
    data = []
    for job in jobs:
        data.append(
            [
                job.url,
                 list(job.image_set.values('url'))[0:10] #limits to 10 for debugging
            ]
        )
    return JsonResponse(data, safe=False)
