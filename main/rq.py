import os
from rq import Queue
from redis_worker import conn

REDIS_URL = os.getenv("REDIS_URL", "redis://localhost:6379/")

scrape_queue = Queue(name="scrape_job", connection=conn)
