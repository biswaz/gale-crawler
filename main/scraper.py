import requests
from bs4 import BeautifulSoup

"""
This crawler does not differentiate external and internal urls.
"""

def fix_url(url):
    if not url.startswith('http'):
        url = 'http://' + url
    if not url.endswith('/'):
        url = url + '/'
    return url

def fix_image_url(url):
    if url.startswith('/'):
        return url[1:] 

def scrape(url, depth):
    # url = "http://books.toscrape.com/"
    import django
    django.setup()
    from main.models import Job, Image

    url = fix_url(url)

    job = Job(url=url)
    job.save()

    max_depth = int(depth)
    scrape_queue = []
    image_list = []
    scrape_queue.append(url)
    scrape_queue.append(None)
    current_depth = 0

    while(scrape_queue and current_depth <= max_depth):
        cur_url = scrape_queue.pop(0)
        if cur_url == None:
            current_depth += 1
        else:
            page = requests.get(cur_url)
            soup = BeautifulSoup(page.text, 'html.parser')

            images = soup.find_all('img')

            for image in images:
                try:
                    if image['src'].startswith('http') or image['src'].startswith('www'):
                        full_url = image['src']
                    else:
                        full_url = fix_url(url) + fix_image_url(image['src'])
                    if full_url not in image_list:
                        image_list.append(full_url)
                        img = Image(url=full_url, job_name=job)
                        img.save()
                except:
                    pass
                    #TODO: handle errors

            ahrefs = soup.find_all('a')

            for a in ahrefs:
                try:
                    if url + a['href'] not in ahrefs:
                        scrape_queue.append(url + a['href'])
                except:
                    pass
            scrape_queue.append(None)

            print("{} images at depth {}".format(len(image_list), current_depth))

