The crawler inputs a seed url and depth. 
The worker is implemented in Redis. Run it by : python redis_worker.py

Core functionality was implemented. Styling is not done. Handling edge cases for urls and proxy rotation is left for the future.